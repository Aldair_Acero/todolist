
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    	String contextPath = request.getContextPath();
    	String message = "Welcome Index page";
    	String title = "Index Page";
    %>
    
    <%!
	 	public String getDate(){
    		return java.util.Calendar.getInstance().getTime().toString();
    	}
    %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><%=title%></title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">
	<div class="row">
		<h1 class="col-12 display-3">To Do List</h1>
	
		<br /><br /><br /><br />
	
		<ul class="list-group col-12">
			<li class="list-item"><a href="views/form.jsp">Form new task</a></li>
		</ul>
	</div>
</div>

</body>
</html>
