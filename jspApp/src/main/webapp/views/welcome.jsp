<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    	String message = "Welcome Aldair Acero";
    	String title = "Welcome Page";
    %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><%=title%></title>
</head>
<body>
<h1><%=message%></h1>
<br>
<a href="../index.jsp">Back</a>
</body>
</html>