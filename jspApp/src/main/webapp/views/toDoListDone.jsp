<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
    
    
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    <%@ page import = "java.util.List" %>
    <%@ page import = "com.softtek.academy.javaweb.model.ToDoList" %>
    <%@ page import = "com.softtek.academy.javaweb.model.Task" %>
    <%@ page import = "com.softtek.academy.javaweb.dao.ObjDao" %>
    
    
    <%
    	int id = (Integer.parseInt(request.getParameter("id")));
    	ObjDao.Update(id);
    	ToDoList to_do_list = new ToDoList();
		List<Task> listaT = to_do_list.getList();
		System.out.println(listaT.get(0).getIs_done());
		session.setAttribute("lista", listaT);
    %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	
	<h1 display-3>To Do List</h1>
<div class="container">
	<div class="row">
		<table class="table">
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
            <c:forEach items="${lista}" var="p" >
                <tr>
                    <c:if test="${p.getIs_done() == 1}">
 						<td><c:out value="${p.getList()}"/></td>
 						<td>Done</td>
					</c:if>
                 </tr>
            </c:forEach>
        </table>  
        
        <a href="http://localhost:8080/jspApp/views/toDoList.jsp">Go to do List</a>
	</div>
</div>
	
	

</body>
</html>