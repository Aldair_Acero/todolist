<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
    
    
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    <%@ page import = "java.util.List" %>
    <%@ page import = "com.softtek.academy.javaweb.model.ToDoList" %>
    <%@ page import = "com.softtek.academy.javaweb.model.Task" %>
    
    
    <%
    	
    	ToDoList to_do_list = new ToDoList();
		List<Task> listaT = to_do_list.getList();
		System.out.println(listaT.get(0).getIs_done());
		session.setAttribute("lista", listaT);
		
    %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<h1 class="display-3">To Do List</h1>

	<table class="table">
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
            <c:forEach items="${lista}" var="p" >
                <tr>
                    <c:if test="${p.getIs_done() == 0}">
 						<td>${p.getList()}</td>
 						<td><a href="http://localhost:8080/jspApp/views/toDoListDone.jsp?id=${p.getId()}">Done</a></td>
					</c:if>
                 </tr>
            </c:forEach>
        </table> 
         <a href="http://localhost:8080/jspApp/views/form.jsp">Go to new Task</a>
		</div>
	</div>
	
</body>
</html>