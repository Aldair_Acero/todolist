package com.softtek.academy.javaweb.dao;

import java.sql.Connection;

@FunctionalInterface
public interface ConexionMysql {
	public Connection conexionI();
}
