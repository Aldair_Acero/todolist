package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.model.UserBean;

/**
 * Servlet implementation class ControllerServlet
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		UserBean user = new UserBean();
		user.setUser(request.getParameter("user"));
		user.setPass(request.getParameter("pass"));
		
		PrintWriter out = response.getWriter();
		
		
		if(user.compare()) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/sesion.jsp");
			request.setAttribute("mensage", "Inicio sesion correctamente!");
			request.setAttribute("user",user);
			dispatcher.forward(request,response);
		}else {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/formMVC.jsp");
			request.setAttribute("mensage", "error de credenciales!");
			dispatcher.forward(request,response);
		}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
