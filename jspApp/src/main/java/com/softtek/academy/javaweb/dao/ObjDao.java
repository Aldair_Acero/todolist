package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.Task;
import com.softtek.academy.javaweb.dao.Conector;

public class ObjDao {
	
	public static List<Task> getLista() {
		
		List<Task> toDoList = new ArrayList<Task>();
		
		
		try{Connection conn = Conector.getConector().conexionI();
        PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM TO_DO_LIST");

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
        	int id = resultSet.getInt("id");
           String list = resultSet.getString("list");
           int is_done = resultSet.getInt("is_done");
           Task obj = new Task();
           obj.setId(id);
           obj.setIs_done(is_done);
           obj.setList(list);
           toDoList.add(obj);
        }
		}catch(Exception e) {
			System.out.println("Error en la conexion select");
		}
            
	return toDoList;
	}
	
	public static void Update(int i) {
		String query = "UPDATE to_do_list set is_done=? where id=?";
		try{Connection conn = Conector.getConector().conexionI();
        PreparedStatement preparedStatement = conn.prepareStatement(query);
        preparedStatement.setInt(1, 1);
        preparedStatement.setInt(2, i);
        preparedStatement.execute();
        System.out.println("Salida Update");
        
		}catch(Exception e) {
			System.out.println("Error en la conexion update");
			System.out.println(e);
		}
	}
	
	public static void ingresar(String name) {
		int is_done = 0;
		try{Connection conn = Conector.getConector().conexionI();
		System.out.println(conn);
		System.out.println("1");
        PreparedStatement preparedStatement = conn.prepareStatement(
        		"INSERT INTO to_do_list (list, is_done) values(?,?);");
        preparedStatement.setString(1,name); 
        preparedStatement.setInt(2,is_done);
        System.out.println("2");
        preparedStatement.execute();
        System.out.println("3");
        System.out.println("Salida ingresar");
        
		}catch(Exception e) {
			System.out.println("Error en la conexion insert");
			System.out.println(e);
		}
	}
	
	
	

}
