package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public final class Conector implements ConexionMysql {
	
	private static final Conector conexion = new Conector();
	
	private Conector() {}
	
	public static Conector getConector(){
		return conexion;
	}

	public Connection conexionI() {
		System.out.println("MySQL JDBC Connection");
		
		final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/jsp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL,USER,PASS);
			
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return con;
	}


	
}
