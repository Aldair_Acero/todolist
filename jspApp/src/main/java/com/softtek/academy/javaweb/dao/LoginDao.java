package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


import com.softtek.academy.javaweb.model.PasatiemposBean;
import com.softtek.academy.javaweb.model.UserBean;

public class LoginDao {
	
	public static Connection getConnection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/jsp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL,USER,PASS);
			
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return con;
	}
	
	public static UserBean getUser(String name) {
		
		UserBean user = null;
		
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM USER WHERE NAME=?");
			ps.setString(1,name);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				user = new UserBean();
				user.setUser(rs.getString("NAME"));
				user.setPass(rs.getString("PASS"));
			}
		}catch(Exception e) {
			System.out.println(e);
			return user;
		}
		return user;
	}
	
	public static ArrayList<PasatiemposBean> getPasatiempo() {
		
		ArrayList <PasatiemposBean> array = new ArrayList<PasatiemposBean>();
		PasatiemposBean bean = null;
		
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM PASATIEMPOS");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				bean = new PasatiemposBean();
				bean.setIndice((rs.getInt("IDPASATIEMPO")));
				bean.setNombre(rs.getString("NOMBRE"));
				array.add(bean);
			}
		}catch(Exception e) {
			System.out.println(e);
			return array;
		}
		return array;
	}
	
}
