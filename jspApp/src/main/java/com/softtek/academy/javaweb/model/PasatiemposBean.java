package com.softtek.academy.javaweb.model;
public class PasatiemposBean {
	
	private int indice;
	private String nombre;
	
	public int getIndice() {
		return indice;
	}
	public void setIndice(int indice) {
		this.indice = indice;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
