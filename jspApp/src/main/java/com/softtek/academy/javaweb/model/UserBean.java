package com.softtek.academy.javaweb.model;

import java.util.ArrayList;

import com.softtek.academy.javaweb.dao.LoginDao;

public class UserBean {
	
	private String user;
	private String pass;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public boolean compare() {
		
		boolean respuesta = false;
		
		UserBean result = LoginDao.getUser(user);
		
		try {
			if(result.getPass().toString().equals(
					this.getPass().toString())) {
				
				respuesta = true;
			}else {
				respuesta = false;
			}
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return respuesta;
	}
	
	public ArrayList<PasatiemposBean>getPasatiempos(){
		
		return LoginDao.getPasatiempo();
	}
}
